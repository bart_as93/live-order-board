### Prerequirements
- Java 22

### How to run project:
```
mvn clean package
docker-compose up -d
```
### How to stop project:
```
docker-compose down
```

### Testing App
Use postman collection in /postman directory


### Description and justification for implementation

1. Memory storage solution vs database - Due to the fact that the application is to be horizontally scalable 
I am implementing it with a database (mysql). The best option for storing Orders would be Kafka, but for simplicity 
I used an SQL database.
2. To make the application more asynchronous, we can use the @Async annotation in the public method (since it uses a
proxy) - it will create a custom thread pool (8 by default) that will process the asynchronous methods. 
But starting Tomcat, we start with a minimum of 10 threads and the queue capacity of 100, so our application is 
asynchronous by default.
3. I implemented a simplified version of extracting summary Orders. I made it possible to extend OrderEntity with 
more unit, currency. However, I simplified extracting from the database for the purpose of the task only to KG and GBP.
4. The query is at the database level, to avoid storing all records in RAM
5. To ensure retransmissions - We can make additional Entity - IdempotencyEntity. In RegisterOrder dto I would add an 
idempotencyKey (Random 32 chars alphanumeric value). IdempotencyEntity (table) would contain String:idempotencyKey and 
timestamp. I would introduce property "app.idempotency.check-in-last-hours: 24" - it would only check the key from the 
past 24 hours. And I would add validation in com.lseg.liveorderboard.controller.OrderController#registerOrder before 
calling the service for example idempotencyService.checkKey(...). If the same idempotencyKey was sent in the last 24 hours
to re-detect retransmissions and if retransmission occurs w would avoid executing of OrderService.register method. 
To simplify the task I omitted that part.