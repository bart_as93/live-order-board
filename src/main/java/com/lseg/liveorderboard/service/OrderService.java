package com.lseg.liveorderboard.service;

import com.lseg.liveorderboard.model.*;
import com.lseg.liveorderboard.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private static final String SORT_PROPERTY_PRICE_PER_UNIT = "pricePerUnit";

    private final OrderRepository orderRepository;

    public Long register(RegisterOrder registerOrder) {
        validate(registerOrder);
        OrderEntity entity = map(registerOrder);
        return orderRepository.save(entity).getId();
    }

    public void cancelOrder(Long id) {
        boolean exists = orderRepository.existsById(id);
        if (exists) {
            log.debug("Deleting order with id: {}", id);
            orderRepository.deleteById(id);
        } else {
            log.debug("Order with id {} already not exists in db", id);
        }
    }

    public List<OrderSummaryItem> fetchSummary(OrderType type) {
        switch (type) {
            case SELL -> {
                return orderRepository.summaryOnlyForKgUnitAndGBPCurrency(OrderType.SELL, Sort.by(Sort.Direction.ASC, SORT_PROPERTY_PRICE_PER_UNIT));
            }
            case BUY -> {
                return orderRepository.summaryOnlyForKgUnitAndGBPCurrency(OrderType.BUY, Sort.by(Sort.Direction.DESC, SORT_PROPERTY_PRICE_PER_UNIT));
            }
            case null, default -> throw new IllegalStateException("Unsupported type value");
        }
    }

    private OrderEntity map(RegisterOrder registerOrder) {
        OrderEntity entity = new OrderEntity();
        entity.setOrderQuantity(registerOrder.orderQuantityInKg());
        entity.setUnit(OrderUnit.KG);
        entity.setType(registerOrder.type());
        entity.setPricePerUnit(registerOrder.pricePerKg());
        entity.setCurrency(Currency.GBP);
        entity.setUserId(registerOrder.userId());
        entity.setTimestamp(Timestamp.from(Instant.now()));
        return entity;
    }

    private void validate(RegisterOrder registerOrder) {
        if (registerOrder == null) {
            throw new IllegalArgumentException("Register order cannot be null");
        }
        if (registerOrder.orderQuantityInKg() ==null || BigDecimal.ZERO.compareTo(registerOrder.orderQuantityInKg()) > 0) {
            throw new IllegalArgumentException("Quantity cannot be less then 0");
        }
        if (registerOrder.pricePerKg() == null || BigDecimal.ZERO.compareTo(registerOrder.pricePerKg()) > 0) {
            throw new IllegalArgumentException("Price cannot be less then 0");
        }
    }
}
