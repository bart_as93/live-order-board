package com.lseg.liveorderboard.controller;

import com.lseg.liveorderboard.model.OrderSummaryItem;
import com.lseg.liveorderboard.model.OrderType;
import com.lseg.liveorderboard.model.RegisterOrder;
import com.lseg.liveorderboard.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping("/order")
    public Long registerOrder(@RequestBody RegisterOrder registerOrder) {
        return orderService.register(registerOrder);
    }

    @GetMapping("/order/summary")
    public List<OrderSummaryItem> fetchSummary(@RequestParam OrderType type) {
        return orderService.fetchSummary(type);
    }

    @DeleteMapping("/order/{id}")
    public void cancelOrder(@PathVariable("id") Long id) {
        orderService.cancelOrder(id);
    }

}
