package com.lseg.liveorderboard.model;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userId;
    private BigDecimal orderQuantity;
    @Enumerated(EnumType.STRING)
    private OrderUnit unit;
    private BigDecimal pricePerUnit;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @Enumerated(EnumType.STRING)
    private OrderType type;
    private Timestamp timestamp;
}