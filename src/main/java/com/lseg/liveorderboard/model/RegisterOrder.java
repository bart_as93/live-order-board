package com.lseg.liveorderboard.model;


import java.math.BigDecimal;

public record RegisterOrder(String userId,
                            OrderType type,
                            BigDecimal orderQuantityInKg,
                            BigDecimal pricePerKg) {
}
