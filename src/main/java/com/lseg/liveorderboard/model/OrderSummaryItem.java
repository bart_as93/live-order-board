package com.lseg.liveorderboard.model;


import java.math.BigDecimal;

public record OrderSummaryItem(BigDecimal pricePerUnit,
                               BigDecimal orderQuantity,
                               Currency currency,
                               OrderUnit unit) {}
