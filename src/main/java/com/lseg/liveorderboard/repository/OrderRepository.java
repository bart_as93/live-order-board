package com.lseg.liveorderboard.repository;

import com.lseg.liveorderboard.model.OrderEntity;
import com.lseg.liveorderboard.model.OrderSummaryItem;
import com.lseg.liveorderboard.model.OrderType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    @Query("""
            SELECT new com.lseg.liveorderboard.model.OrderSummaryItem(
                o.pricePerUnit as pricePerUnit, 
                sum(o.orderQuantity) as orderQuantity, 
                o.currency as currency, 
                o.unit as unit) 
            FROM OrderEntity o 
            WHERE o.type=:type AND o.unit='KG' AND o.currency='GBP' 
            GROUP BY o.pricePerUnit
            """)
    List<OrderSummaryItem> summaryOnlyForKgUnitAndGBPCurrency(@Param("type") OrderType type, Sort sort);
}

