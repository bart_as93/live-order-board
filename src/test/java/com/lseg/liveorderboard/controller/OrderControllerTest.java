package com.lseg.liveorderboard.controller;

import com.lseg.liveorderboard.model.OrderEntity;
import com.lseg.liveorderboard.model.OrderSummaryItem;
import com.lseg.liveorderboard.model.OrderType;
import com.lseg.liveorderboard.model.RegisterOrder;
import com.lseg.liveorderboard.repository.OrderRepository;
import com.lseg.liveorderboard.service.OrderService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;

    @AfterEach
    void tearDown() {
        orderRepository.deleteAll();
    }

    @Test
    public void shouldRegisterOrder() throws Exception {
        // given
        String userId = "user1";
        OrderType orderType = OrderType.SELL;
        BigDecimal orderQuantityInKg = new BigDecimal("3.5");
        BigDecimal pricePerKg = new BigDecimal("306");
        RegisterOrder registerOrder = new RegisterOrder(userId, orderType, orderQuantityInKg, pricePerKg);

        // when
        ResponseEntity<Long> response = restTemplate.postForEntity("/order", registerOrder, Long.class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        Long orderId = response.getBody();
        assertThat(orderId).isNotNull();
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(orderId);
        assertThat(optionalOrderEntity).isNotEmpty();
        OrderEntity orderEntity = optionalOrderEntity.get();
        assertThat(orderEntity).isNotNull();
        assertThat(orderEntity.getUserId()).isEqualTo(userId);
        assertThat(orderEntity.getType()).isEqualTo(orderType);
        assertThat(orderEntity.getPricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKg);
        assertThat(orderEntity.getOrderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKg);
    }

    @Test
    public void shouldNotRegisterOrderWhenInvalidRequestBody() throws Exception {
        // given
        String userId = "user1";
        OrderType orderType = OrderType.SELL;
        RegisterOrder registerOrder = new RegisterOrder(userId, orderType, null, null);

        // when
        // then
        assertThrows(RestClientException.class,
                () -> restTemplate.postForEntity("/order", registerOrder, Long.class));
    }

    @Test
    public void shouldFetchSummaryForSale() throws Exception {
        // given
        BigDecimal pricePerKgInOrder1AndOrder4 = new BigDecimal("306");
        BigDecimal pricePerKgInOrder2 = new BigDecimal("310");
        BigDecimal pricePerKgInOrder3 = new BigDecimal("307");
        BigDecimal orderQuantityInKgInOrder1 = new BigDecimal("3.5");
        BigDecimal orderQuantityInKgInOrder2 = new BigDecimal("1.2");
        BigDecimal orderQuantityInKgInOrder3 = new BigDecimal("1.5");
        BigDecimal orderQuantityInKgInOrder4 = new BigDecimal("2.0");

        orderService.register(new RegisterOrder("user1", OrderType.SELL, orderQuantityInKgInOrder1, pricePerKgInOrder1AndOrder4));
        orderService.register(new RegisterOrder("user2", OrderType.SELL, orderQuantityInKgInOrder2, pricePerKgInOrder2));
        orderService.register(new RegisterOrder("user3", OrderType.SELL, orderQuantityInKgInOrder3, pricePerKgInOrder3));
        orderService.register(new RegisterOrder("user4", OrderType.SELL, orderQuantityInKgInOrder4, pricePerKgInOrder1AndOrder4));

        // when
        ResponseEntity<OrderSummaryItem[]> response = restTemplate.getForEntity("/order/summary?type=SELL", OrderSummaryItem[].class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        OrderSummaryItem[] summary = response.getBody();
        assertThat(summary).isNotEmpty();

        assertThat(summary.length).isEqualTo(3);

        OrderSummaryItem firstOrderSummary = summary[0];
        assertThat(firstOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder1AndOrder4);
        assertThat(firstOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder1.add(orderQuantityInKgInOrder4));

        OrderSummaryItem secondOrderSummary = summary[1];
        assertThat(secondOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder3);
        assertThat(secondOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder3);

        OrderSummaryItem thirdOrderSummary = summary[2];
        assertThat(thirdOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder2);
        assertThat(thirdOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder2);
    }

    @Test
    public void shouldFetchSummaryForBuy() throws Exception {
        // given
        BigDecimal pricePerKgInOrder1AndOrder4 = new BigDecimal("306");
        BigDecimal pricePerKgInOrder2 = new BigDecimal("310");
        BigDecimal pricePerKgInOrder3 = new BigDecimal("307");
        BigDecimal orderQuantityInKgInOrder1 = new BigDecimal("3.5");
        BigDecimal orderQuantityInKgInOrder2 = new BigDecimal("1.2");
        BigDecimal orderQuantityInKgInOrder3 = new BigDecimal("1.5");
        BigDecimal orderQuantityInKgInOrder4 = new BigDecimal("2.0");

        orderService.register(new RegisterOrder("user1", OrderType.BUY, orderQuantityInKgInOrder1, pricePerKgInOrder1AndOrder4));
        orderService.register(new RegisterOrder("user2", OrderType.BUY, orderQuantityInKgInOrder2, pricePerKgInOrder2));
        orderService.register(new RegisterOrder("user3", OrderType.BUY, orderQuantityInKgInOrder3, pricePerKgInOrder3));
        orderService.register(new RegisterOrder("user4", OrderType.BUY, orderQuantityInKgInOrder4, pricePerKgInOrder1AndOrder4));

        // when
        ResponseEntity<OrderSummaryItem[]> response = restTemplate.getForEntity("/order/summary?type=BUY", OrderSummaryItem[].class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        OrderSummaryItem[] summary = response.getBody();
        assertThat(summary).isNotEmpty();

        assertThat(summary.length).isEqualTo(3);

        OrderSummaryItem firstOrderSummary = summary[0];
        assertThat(firstOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder2);
        assertThat(firstOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder2);

        OrderSummaryItem secondOrderSummary = summary[1];
        assertThat(secondOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder3);
        assertThat(secondOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder3);

        OrderSummaryItem thirdOrderSummary = summary[2];
        assertThat(thirdOrderSummary.pricePerUnit()).usingComparator(BigDecimal::compareTo).isEqualTo(pricePerKgInOrder1AndOrder4);
        assertThat(thirdOrderSummary.orderQuantity()).usingComparator(BigDecimal::compareTo).isEqualTo(orderQuantityInKgInOrder1.add(orderQuantityInKgInOrder4));
    }

    @Test
    public void shouldCancelOrder() throws Exception {
        // given
        RegisterOrder registerOrder = new RegisterOrder("user1", OrderType.SELL, new BigDecimal("3.5"), new BigDecimal("306"));
        Long orderId = orderService.register(registerOrder);

        // when
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Void> response = restTemplate.exchange("/order/" + orderId, HttpMethod.DELETE, entity, Void.class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(orderId);
        assertThat(optionalOrderEntity).isEmpty();
    }

    @Test
    public void shouldCancelNotExistingOrder() throws Exception {
        // given
        long orderId = 1L;

        // when
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Void> response = restTemplate.exchange("/order/" + orderId, HttpMethod.DELETE, entity, Void.class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(orderId);
        assertThat(optionalOrderEntity).isEmpty();
    }
}